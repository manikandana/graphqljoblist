import Head from 'next/head'
import Header from './theme/header'

import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql
} from "@apollo/client";
import Joblisting from './component/joblisting';


export default function Home({data}) {
  return (
    <div className="flex flex-col  min-h-screen ">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header></Header>
     <Joblisting jobdata={data}></Joblisting>
    

      <footer className="flex items-center justify-center w-full h-24 border-t">
        <a
          className="flex items-center justify-center"
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className="h-4 ml-2" />
        </a>
      </footer>
    </div>
  )
}


export async function getServerSideProps() {
  // Fetch data from external API
  const client = new ApolloClient({
    uri: 'https://api.graphql.jobs/',
    cache: new InMemoryCache()
  });

  const { data } = await client.query({
    query: gql`
      query Jobs {
        jobs {
          id
  title
  slug
  locationNames
  isFeatured
  tags{
    name
  }
  cities{
    name
  }
   company{
    name
    slug
    logoUrl
    websiteUrl
  }
        }
      }
    `,
  });

  // Pass data to the page via props
  return { props: { data } }
}
