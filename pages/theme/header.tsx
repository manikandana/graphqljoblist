
export default function Header(){
    return(
        <div className="bggradient h-96 ">

 <img src="/svg0.svg"  className="absolute -z-10" />
   
<div className="flex-row pt-10" >  
    <a className="float-left pl-3" href='/' > <img src="/graphqllogo.svg" className='float-left pt-1 pr-1' /> GraphQL.jobs</a> 
    <a className="float-right pr-3" href='https://graphql.jobs/post-your-job'>Post your job</a>
</div>

      <main className="flex flex-col items-center justify-center w-full flex-1 pt-24 text-center">
     
<h1 className="text-4xl font-bold">
        Work with <span className='text-white'> GraphQL </span>
        </h1>
        <h1 className="text-4xl font-bold">
        in a modern startup.
        </h1>

        
        <div className="flex items-center justify-center " >
    <div className="flex border-2 border-gray-200 rounded">
        <input type="text" className="px-4 py-2 w-80" placeholder="Search..."></input>
        <button className="px-4 text-white bg-gray-600 border-l ">
            Search
        </button>
    </div>

</div>
<h3 className="text-sm  pt-5">
We are supported by  <a href="https://atlasmic.com" target="_blank">Atlasmic's live chat</a> platform
        </h3>

        
        
       
       
      </main>
      </div>
       
    )
}