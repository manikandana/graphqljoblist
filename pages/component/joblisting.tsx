export default function Joblisting({jobdata})
{

return(

<div>
Older than a Year
<table className="table-auto" width="100%">
  
  <tbody>
{
     jobdata.jobs.map((row)=>{return <tr className={row.isFeatured?"bg-red-50 h-11":"h-11"}>
       <td><img src={row.company.websiteUrl !=null && row.logoUrl ==null?"https://logo.clearbit.com/"+ row.company.websiteUrl.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0] +"?size=200":row.logoUrl} className="object-fill h-10 w-10 " ></img> </td>
        <td>{row.title}</td>
        <td>{row.tags.map((tagdata,index)=>{  if(index<2) return <div className="flex flex-row">{tagdata.name}</div>})}</td>
      <td>{ row.locationNames==null?<i>Remote</i>:row.locationNames}</td> </tr> })
}
  </tbody>
</table>



</div>

)

}